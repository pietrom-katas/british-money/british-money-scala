package org.amicofragile.british.money

import org.amicofragile.british.money.BritishMoney.{PenniesPerPound, PenniesPerShilling}

case class BritishMoney private (private val totalPennies: Int) {
  def this(pounds: Int, shillings: Int, pennies: Int) = {
    this(pounds * PenniesPerPound + shillings * PenniesPerShilling + pennies)
  }

  def pennies = totalPennies % PenniesPerShilling

  def shillings = (totalPennies % PenniesPerPound) / PenniesPerShilling

  def pounds = totalPennies / PenniesPerPound

  def +(that: BritishMoney) = new BritishMoney(this.pounds + that.pounds, this.shillings + that.shillings, this.pennies + that.pennies)

  def /(n: Int): (BritishMoney, BritishMoney) = {
    val result = totalPennies / n
    val rest = totalPennies % n
    (new BritishMoney(result), new BritishMoney(rest))
  }
}

object BritishMoney {
  val PenniesPerShilling = 12
  val ShillingsPerPound = 20
  val PenniesPerPound = PenniesPerShilling * ShillingsPerPound
}