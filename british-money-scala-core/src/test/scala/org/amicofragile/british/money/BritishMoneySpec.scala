package org.amicofragile.british.money

import org.scalatest.funspec.AnyFunSpec

class BritishMoneySpec extends AnyFunSpec {
  describe("BritishMoney") {
    it("can be instantiated providing three integer values and exposes those values") {
      val money = new BritishMoney(1, 1, 1)
      assert(money.pennies == 1)
      assert(money.shillings == 1)
      assert(money.pounds == 1)
    }

    it("can be added to another BritishMoney, handling overflow") {
      val result = new BritishMoney(5, 17, 8) + new BritishMoney(3,4,10)
      assert(result.pounds == 9)
      assert(result.shillings == 2)
      assert(result.pennies == 6)
    }

    it("can be divided by Int") {
      val (result, rest) = new BritishMoney(18, 16, 1) / 15
      assert(result == new BritishMoney(1, 5, 0))
      assert(rest == new BritishMoney(0, 1, 1))

    }
  }
}